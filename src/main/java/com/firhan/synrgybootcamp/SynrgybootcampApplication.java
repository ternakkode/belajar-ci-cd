package com.firhan.synrgybootcamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SynrgybootcampApplication {

	public static void main(String[] args) {
		SpringApplication.run(SynrgybootcampApplication.class, args);
	}

}
