package com.firhan.synrgybootcamp.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Waktu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "detail")
    String detail;

    @OneToMany(mappedBy = "waktu")
    Set<Jadwal> jadwal;
}
