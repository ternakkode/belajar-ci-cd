package com.firhan.synrgybootcamp.entity;

import lombok.*;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Jadwal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Long id;

    @Enumerated(EnumType.ORDINAL)
    Hari hari;

    @ManyToOne
    @JoinColumn(name = "kelas_id")
    Kelas kelas;

    @ManyToOne
    @JoinColumn(name = "mata_pelajaran_id")
    MataPelajaran mataPelajaran;

    @ManyToOne
    @JoinColumn(name = "pendidik_id")
    Pendidik pendidik;

    @ManyToOne
    @JoinColumn(name = "waktu_id")
    Waktu waktu;
}
