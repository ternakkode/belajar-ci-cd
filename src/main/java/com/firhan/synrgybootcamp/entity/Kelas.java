package com.firhan.synrgybootcamp.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table
public class Kelas {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nama")
    private String nama;

    @OneToMany
    Set<Siswa> siswa;

    @OneToMany(mappedBy = "kelas")
    Set<Jadwal> jadwal;
}
