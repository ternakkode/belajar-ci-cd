package com.firhan.synrgybootcamp.presenter;

import com.firhan.synrgybootcamp.presenter.model.request.KelasRequest;
import com.firhan.synrgybootcamp.presenter.model.response.DetailKelasResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListKelasResponse;
import com.firhan.synrgybootcamp.service.KelasService;
import com.firhan.synrgybootcamp.util.Pagination;
import com.firhan.synrgybootcamp.util.ResponseFormat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/kelas")
public class KelasPresenter {

    KelasService kelasService;

    public KelasPresenter(KelasService kelasService) {
        this.kelasService = kelasService;
    }

    @PostMapping
    public ResponseEntity<ResponseFormat> createKelas(
            @RequestBody KelasRequest payload
    ) {
        ListKelasResponse result = kelasService.createKelas(payload);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil menambahkan kelas baru")
                        .data(result)
                        .build(), HttpStatus.CREATED
        );
    }

    @GetMapping
    public ResponseEntity<ResponseFormat> getAllKelas(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "8") int size,
            @RequestParam(defaultValue = "id") String sortBy,
            @RequestParam(defaultValue = "desc") String sortOrder
    ) {
        Page<ListKelasResponse> pagedKelas = kelasService.getAllKelas(
                PageRequest.of(
                        page,
                        size,
                        sortOrder.equals("desc") ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending()
                )
        );

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengambil data kelas")
                        .data(pagedKelas.getContent())
                        .pagination(new Pagination(pagedKelas))
                        .build(), HttpStatus.OK
        );
    }

    @GetMapping({"name"})
    public ResponseEntity<ResponseFormat> getKelasByName(
            @PathVariable String name
    ) {
        DetailKelasResponse result = kelasService.getKelasByNama(name);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengambil data kelas")
                        .data(result)
                        .build(), HttpStatus.OK
        );
    }

    @PutMapping("{id}")
    public ResponseEntity<ResponseFormat> updateKelasById(
            @PathVariable Long id,
            @RequestBody KelasRequest payload
    ) {
        DetailKelasResponse result = kelasService.updateKelasById(id, payload);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengubah data kelas")
                        .data(result)
                        .build(), HttpStatus.OK
        );
    }

    @DeleteMapping("{id}")
    public ResponseEntity<ResponseFormat> deleteKelasById(
            @PathVariable Long id
    ) {
        kelasService.deleteKelasById(id);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil menghapus data kelas")
                        .build(), HttpStatus.OK
        );
    }
}
