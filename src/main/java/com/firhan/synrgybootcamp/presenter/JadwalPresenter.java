package com.firhan.synrgybootcamp.presenter;

import com.firhan.synrgybootcamp.presenter.model.request.JadwalRequest;
import com.firhan.synrgybootcamp.presenter.model.response.JadwalResponse;
import com.firhan.synrgybootcamp.service.JadwalService;
import com.firhan.synrgybootcamp.util.Pagination;
import com.firhan.synrgybootcamp.util.ResponseFormat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/jadwal")
public class JadwalPresenter {

    JadwalService jadwalService;

    public JadwalPresenter(JadwalService jadwalService) {
        this.jadwalService = jadwalService;
    }

    @PostMapping
    public ResponseEntity<ResponseFormat> createJadwal(
            @RequestBody JadwalRequest payload
    ) {
        JadwalResponse result = jadwalService.createJadwal(payload);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil menambahkan jadwal baru")
                        .data(result)
                        .build(), HttpStatus.CREATED
        );
    }

    @GetMapping
    public ResponseEntity<ResponseFormat> getAllKelas(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "8") int size,
            @RequestParam(defaultValue = "id") String sortBy,
            @RequestParam(defaultValue = "desc") String sortOrder
    ) {
        Page<JadwalResponse> pagedJadwal = jadwalService.getAllJadwal(
                PageRequest.of(
                        page,
                        size,
                        sortOrder.equals("desc") ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending()
                )
        );

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengambil data jadwal")
                        .data(pagedJadwal.getContent())
                        .pagination(new Pagination(pagedJadwal))
                        .build(), HttpStatus.OK
        );
    }

    @GetMapping("{id}")
    public ResponseEntity<ResponseFormat> getJadwalById(
            @PathVariable Long id
    ) {
        JadwalResponse result = jadwalService.getJadwalById(id);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengambil data jadwal")
                        .data(result)
                        .build(), HttpStatus.OK
        );
    }

    @PutMapping("{id}")
    public ResponseEntity<ResponseFormat> updateJadwalById(
            @PathVariable Long id,
            @RequestBody JadwalRequest payload
    ) {
        JadwalResponse result = jadwalService.updateJadwalById(id, payload);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengubah data jadwal")
                        .data(result)
                        .build(), HttpStatus.OK
        );
    }

    @DeleteMapping("{id}")
    public ResponseEntity<ResponseFormat> deleteJadwalById(
            @PathVariable Long id
    ) {
        jadwalService.deleteJadwalByid(id);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil menghapus data kelas")
                        .build(), HttpStatus.OK
        );
    }
}
