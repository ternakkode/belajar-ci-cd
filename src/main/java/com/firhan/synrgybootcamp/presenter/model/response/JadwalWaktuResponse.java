package com.firhan.synrgybootcamp.presenter.model.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class JadwalWaktuResponse {
    String hari;
    String kelas;
    String mataPelajaran;
    String pendidik;
}
