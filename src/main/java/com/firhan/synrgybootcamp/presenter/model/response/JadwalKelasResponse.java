package com.firhan.synrgybootcamp.presenter.model.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class JadwalKelasResponse {
    String hari;
    String waktu;
    String mataPelajaran;
    String pendidik;
}
