package com.firhan.synrgybootcamp.presenter.model.response;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Builder
@Data
public class DetailMataPelajaranResponse {
    Long id;
    String nama;
    Set<JadwalMataPelajaranResponse> jadwal;
}
