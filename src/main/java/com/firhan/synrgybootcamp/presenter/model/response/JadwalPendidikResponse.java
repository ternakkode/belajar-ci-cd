package com.firhan.synrgybootcamp.presenter.model.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class JadwalPendidikResponse {
    String hari;
    String waktu;
    String kelas;
    String mataPelajaran;
}
