package com.firhan.synrgybootcamp.presenter.model.response;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Builder
@Data
public class DetailWaktuResponse {
    Long id;
    String detail;
    Set<JadwalWaktuResponse> jadwal;
}
