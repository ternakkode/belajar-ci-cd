package com.firhan.synrgybootcamp.presenter.model.response;

import com.firhan.synrgybootcamp.entity.Kelas;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Builder
@Data
public class DetailSiswaResponse {
    Long id;
    String nama;
    ListKelasResponse kelas;
    Set<JadwalSiswaResponse> jadwal;
}
