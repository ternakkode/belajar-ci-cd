package com.firhan.synrgybootcamp.presenter.model.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ListWaktuResponse {
    Long id;
    String detail;
}
