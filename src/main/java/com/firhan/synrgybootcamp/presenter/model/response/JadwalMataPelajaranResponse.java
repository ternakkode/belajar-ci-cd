package com.firhan.synrgybootcamp.presenter.model.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class JadwalMataPelajaranResponse {
    String hari;
    String waktu;
    String kelas;
    String pendidik;
}
