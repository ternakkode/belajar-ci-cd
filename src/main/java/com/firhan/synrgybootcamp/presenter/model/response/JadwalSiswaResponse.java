package com.firhan.synrgybootcamp.presenter.model.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class JadwalSiswaResponse {
    String hari;
    String waktu;
    String kelas;
    String pendidik;
    String mataPelajaran;
}
