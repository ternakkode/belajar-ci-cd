package com.firhan.synrgybootcamp.presenter.model.response;

import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Builder
@Data
public class DetailPendidikResponse {
    Long id;
    String nama;
    Set<JadwalPendidikResponse> jadwal;
}
