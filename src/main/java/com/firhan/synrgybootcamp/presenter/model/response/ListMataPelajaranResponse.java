package com.firhan.synrgybootcamp.presenter.model.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ListMataPelajaranResponse {
    Long id;
    String nama;
}
