package com.firhan.synrgybootcamp.presenter.model.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class JadwalResponse {
    Long id;
    String hari;
    ListWaktuResponse waktu;
    ListKelasResponse kelas;
    ListMataPelajaranResponse mataPelajaran;
    ListPendidikResponse pendidik;
}
