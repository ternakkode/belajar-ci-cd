package com.firhan.synrgybootcamp.presenter.model.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class JadwalRequest {
    Long kelasId;
    Long mataPelajaranId;
    Long pendidikId;
    String hari;
    Long waktuId;
}
