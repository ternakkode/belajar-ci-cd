package com.firhan.synrgybootcamp.presenter;

import com.firhan.synrgybootcamp.presenter.model.request.PendidikRequest;
import com.firhan.synrgybootcamp.presenter.model.response.DetailPendidikResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListPendidikResponse;
import com.firhan.synrgybootcamp.service.PendidikService;
import com.firhan.synrgybootcamp.util.Pagination;
import com.firhan.synrgybootcamp.util.ResponseFormat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/pendidik")
public class PendidikPresenter {

    PendidikService pendidikService;

    public PendidikPresenter(PendidikService pendidikService) {
        this.pendidikService = pendidikService;
    }

    @PostMapping
    public ResponseEntity<ResponseFormat> createPendidik(
            @RequestBody PendidikRequest payload
    ) {
        ListPendidikResponse result = pendidikService.createPendidik(payload);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil menambahkan pendidik baru")
                        .data(result)
                        .build(), HttpStatus.CREATED
        );
    }

    @GetMapping
    public ResponseEntity<ResponseFormat> getAllPendidik(
            @RequestParam(required = false) String nama,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "8") int size,
            @RequestParam(defaultValue = "id") String sortBy,
            @RequestParam(defaultValue = "desc") String sortOrder
    ) {
        Page<ListPendidikResponse> pagedPendidik = pendidikService.getAllPendidik(
                nama,
                PageRequest.of(
                        page,
                        size,
                        sortOrder.equals("desc") ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending()
                )
        );

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengambil data pendidik")
                        .data(pagedPendidik.getContent())
                        .pagination(new Pagination(pagedPendidik))
                        .build(), HttpStatus.OK
        );
    }

    @GetMapping("{id}")
    public ResponseEntity<ResponseFormat> getPendidikById(
            @PathVariable Long id
    ) {
        DetailPendidikResponse result = pendidikService.getPendidikById(id);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengambil data pendidik")
                        .data(result)
                        .build(), HttpStatus.OK
        );
    }

    @PutMapping("{id}")
    public ResponseEntity<ResponseFormat> updatePendidikById(
            @PathVariable Long id,
            @RequestBody PendidikRequest payload
    ) {
        DetailPendidikResponse result = pendidikService.updatePendidikById(id, payload);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengubah data pendidik")
                        .data(result)
                        .build(), HttpStatus.OK
        );
    }

    @DeleteMapping("{id}")
    public ResponseEntity<ResponseFormat> deletePendidikById(
            @PathVariable Long id
    ) {
        pendidikService.deletePendidikByid(id);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil menghapus data pendidik")
                        .build(), HttpStatus.OK
        );
    }
}
