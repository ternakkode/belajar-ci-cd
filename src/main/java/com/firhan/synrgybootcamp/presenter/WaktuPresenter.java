package com.firhan.synrgybootcamp.presenter;

import com.firhan.synrgybootcamp.presenter.model.request.WaktuRequest;
import com.firhan.synrgybootcamp.presenter.model.response.DetailWaktuResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListWaktuResponse;
import com.firhan.synrgybootcamp.service.WaktuService;
import com.firhan.synrgybootcamp.util.Pagination;
import com.firhan.synrgybootcamp.util.ResponseFormat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/waktu")
public class WaktuPresenter {

    WaktuService waktuService;

    public WaktuPresenter(WaktuService waktuService) {
        this.waktuService = waktuService;
    }

    @PostMapping
    public ResponseEntity<ResponseFormat> createWaktu(
            @RequestBody WaktuRequest payload
    ) {
        ListWaktuResponse result = waktuService.createWaktu(payload);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil menambahkan waktu baru")
                        .data(result)
                        .build(), HttpStatus.CREATED
        );
    }

    @GetMapping
    public ResponseEntity<ResponseFormat> getAllWaktu(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "8") int size,
            @RequestParam(defaultValue = "id") String sortBy,
            @RequestParam(defaultValue = "desc") String sortOrder
    ) {
        Page<ListWaktuResponse> pagedWaktu = waktuService.getAllWaktu(
                PageRequest.of(
                        page,
                        size,
                        sortOrder.equals("desc") ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending()
                )
        );

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengambil data waktu")
                        .data(pagedWaktu.getContent())
                        .pagination(new Pagination(pagedWaktu))
                        .build(), HttpStatus.OK
        );
    }

    @GetMapping("{id}")
    public ResponseEntity<ResponseFormat> getWaktuById(
            @PathVariable Long id
    ) {
        DetailWaktuResponse result = waktuService.getWaktuById(id);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengambil data waktu")
                        .data(result)
                        .build(), HttpStatus.OK
        );
    }

    @PutMapping("{id}")
    public ResponseEntity<ResponseFormat> updateWaktuById(
            @PathVariable Long id,
            @RequestBody WaktuRequest payload
    ) {
        DetailWaktuResponse result = waktuService.updateWaktuById(id, payload);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengubah data waktu")
                        .data(result)
                        .build(), HttpStatus.OK
        );
    }

    @DeleteMapping("{id}")
    public ResponseEntity<ResponseFormat> deleteWaktuById(
            @PathVariable Long id
    ) {
        waktuService.deleteWaktuById(id);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil menghapus data waktu")
                        .build(), HttpStatus.OK
        );
    }
}
