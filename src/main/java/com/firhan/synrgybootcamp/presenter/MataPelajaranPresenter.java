package com.firhan.synrgybootcamp.presenter;

import com.firhan.synrgybootcamp.presenter.model.request.MataPelajaranRequest;
import com.firhan.synrgybootcamp.presenter.model.response.DetailMataPelajaranResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListMataPelajaranResponse;
import com.firhan.synrgybootcamp.service.MataPelajaranService;
import com.firhan.synrgybootcamp.util.Pagination;
import com.firhan.synrgybootcamp.util.ResponseFormat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/mata-pelajaran")
public class MataPelajaranPresenter {

    MataPelajaranService mataPelajaranService;

    public MataPelajaranPresenter(MataPelajaranService mataPelajaranService) {
        this.mataPelajaranService = mataPelajaranService;
    }

    @PostMapping
    public ResponseEntity<ResponseFormat> createMataPelajaran(
            @RequestBody MataPelajaranRequest payload
    ) {
        ListMataPelajaranResponse result = mataPelajaranService.createMataPelajaran(payload);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil menambahkan mata pelajaran baru")
                        .data(result)
                        .build(), HttpStatus.CREATED
        );
    }

    @GetMapping
    public ResponseEntity<ResponseFormat> getAllMataPelajaran(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "8") int size,
            @RequestParam(defaultValue = "id") String sortBy,
            @RequestParam(defaultValue = "desc") String sortOrder
    ) {
        Page<ListMataPelajaranResponse> pagedMataPelajaran = mataPelajaranService.getAllMataPelajaran(
                PageRequest.of(
                        page,
                        size,
                        sortOrder.equals("desc") ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending()
                )
        );

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengambil data mata pelajaran")
                        .data(pagedMataPelajaran.getContent())
                        .pagination(new Pagination(pagedMataPelajaran))
                        .build(), HttpStatus.OK
        );
    }

    @GetMapping("{id}")
    public ResponseEntity<ResponseFormat> getMataPelajaranById(
            @PathVariable Long id
    ) {
        DetailMataPelajaranResponse result = mataPelajaranService.getMataPelajaranById(id);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengambil data mata pelajaran")
                        .data(result)
                        .build(), HttpStatus.OK
        );
    }

    @PutMapping("{id}")
    public ResponseEntity<ResponseFormat> updateMataPelajaranById(
            @PathVariable Long id,
            @RequestBody MataPelajaranRequest payload
    ) {
        DetailMataPelajaranResponse result = mataPelajaranService.updateMataPelajaranByiD(id, payload);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengubah data mata pelajaran")
                        .data(result)
                        .build(), HttpStatus.OK
        );
    }

    @DeleteMapping("{id}")
    public ResponseEntity<ResponseFormat> deletMataPelajaranById(
            @PathVariable Long id
    ) {
        mataPelajaranService.deleteMataPelajaranById(id);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil menghapus data mata pelajaran")
                        .build(), HttpStatus.OK
        );
    }
}
