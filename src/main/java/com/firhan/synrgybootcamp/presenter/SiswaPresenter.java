package com.firhan.synrgybootcamp.presenter;

import com.firhan.synrgybootcamp.presenter.model.request.SiswaRequest;
import com.firhan.synrgybootcamp.presenter.model.response.DetailSiswaResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListSiswaResponse;
import com.firhan.synrgybootcamp.service.SiswaService;
import com.firhan.synrgybootcamp.util.Pagination;
import com.firhan.synrgybootcamp.util.ResponseFormat;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/v1/siswa")
public class SiswaPresenter {

    SiswaService siswaService;

    public SiswaPresenter(SiswaService siswaService) {
        this.siswaService = siswaService;
    }

    @PostMapping
    public ResponseEntity<ResponseFormat> createSiswa(
            @RequestBody SiswaRequest payload
    ) {
        ListSiswaResponse result = siswaService.createSiswa(payload);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil menambahkan siswa baru")
                        .data(result)
                        .build(), HttpStatus.CREATED
        );
    }

    @GetMapping
    public ResponseEntity<ResponseFormat> getAllSiswa(
            @RequestParam(required = false) String nama,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "8") int size,
            @RequestParam(defaultValue = "id") String sortBy,
            @RequestParam(defaultValue = "desc") String sortOrder
    ) {
        Page<ListSiswaResponse> pagedSiswa = siswaService.getAllSiswa(
                nama,
                PageRequest.of(
                        page,
                        size,
                        sortOrder.equals("desc") ? Sort.by(sortBy).descending() : Sort.by(sortBy).ascending()
                )
        );

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengambil data siswa")
                        .data(pagedSiswa.getContent())
                        .pagination(new Pagination(pagedSiswa))
                        .build(), HttpStatus.OK
        );
    }

    @GetMapping("{id}")
    public ResponseEntity<ResponseFormat> getSiswaById(
            @PathVariable Long id
    ) {
        DetailSiswaResponse result = siswaService.getSiswaById(id);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengambil data siswa")
                        .data(result)
                        .build(), HttpStatus.OK
        );
    }

    @PutMapping("{id}")
    public ResponseEntity<ResponseFormat> updateSiswaById(
            @PathVariable Long id,
            @RequestBody SiswaRequest payload
    ) {
        DetailSiswaResponse result = siswaService.updateSiswaById(id, payload);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil mengubah data siswa")
                        .data(result)
                        .build(), HttpStatus.OK
        );
    }

    @DeleteMapping("{id}")
    public ResponseEntity<ResponseFormat> deleteSiswaById(
            @PathVariable Long id
    ) {
        siswaService.deleteSiswaById(id);

        return new ResponseEntity<>(
                ResponseFormat.builder()
                        .message("berhasil menghapus data siswa")
                        .build(), HttpStatus.OK
        );
    }
}
