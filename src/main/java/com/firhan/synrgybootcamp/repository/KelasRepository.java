package com.firhan.synrgybootcamp.repository;

import com.firhan.synrgybootcamp.entity.Kelas;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface KelasRepository extends JpaRepository<Kelas, Long> {
    Optional<Kelas> findFirstByNama(String nama);
}
