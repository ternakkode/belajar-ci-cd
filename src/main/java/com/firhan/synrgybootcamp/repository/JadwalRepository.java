package com.firhan.synrgybootcamp.repository;

import com.firhan.synrgybootcamp.entity.Jadwal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JadwalRepository extends JpaRepository<Jadwal, Long> {
}
