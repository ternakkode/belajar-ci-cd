package com.firhan.synrgybootcamp.repository;

import com.firhan.synrgybootcamp.entity.Pendidik;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PendidikRepository extends JpaRepository<Pendidik, Long> {
    Page<Pendidik> findByNama(String nama, Pageable page);
}
