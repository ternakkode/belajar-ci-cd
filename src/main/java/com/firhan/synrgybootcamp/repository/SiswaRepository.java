package com.firhan.synrgybootcamp.repository;

import com.firhan.synrgybootcamp.entity.Siswa;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SiswaRepository extends JpaRepository<Siswa, Long> {
    Page<Siswa> findByNama(String nama, Pageable page);
}
