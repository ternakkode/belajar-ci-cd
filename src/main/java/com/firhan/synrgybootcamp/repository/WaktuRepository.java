package com.firhan.synrgybootcamp.repository;

import com.firhan.synrgybootcamp.entity.Waktu;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WaktuRepository extends JpaRepository<Waktu, Long> {

}
