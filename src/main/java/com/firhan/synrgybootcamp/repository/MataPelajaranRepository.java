package com.firhan.synrgybootcamp.repository;

import com.firhan.synrgybootcamp.entity.MataPelajaran;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MataPelajaranRepository extends JpaRepository<MataPelajaran, Long> {
}
