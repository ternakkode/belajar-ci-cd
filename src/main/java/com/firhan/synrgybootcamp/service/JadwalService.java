package com.firhan.synrgybootcamp.service;

import com.firhan.synrgybootcamp.presenter.model.request.JadwalRequest;
import com.firhan.synrgybootcamp.presenter.model.response.JadwalResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface JadwalService {
    JadwalResponse createJadwal(JadwalRequest payload);
    Page<JadwalResponse> getAllJadwal(Pageable page);
    JadwalResponse getJadwalById(Long id);
    JadwalResponse updateJadwalById(Long id, JadwalRequest payload);
    void deleteJadwalByid(Long id);
}
