package com.firhan.synrgybootcamp.service;

import com.firhan.synrgybootcamp.presenter.model.request.PendidikRequest;
import com.firhan.synrgybootcamp.presenter.model.response.DetailPendidikResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListPendidikResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PendidikService {
    ListPendidikResponse createPendidik(PendidikRequest payload);
    Page<ListPendidikResponse> getAllPendidik(String nama, Pageable page);
    DetailPendidikResponse getPendidikById(Long id);
    DetailPendidikResponse updatePendidikById(Long id, PendidikRequest payload);
    void deletePendidikByid(Long id);
}
