package com.firhan.synrgybootcamp.service;

import com.firhan.synrgybootcamp.presenter.model.request.KelasRequest;
import com.firhan.synrgybootcamp.presenter.model.response.DetailKelasResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListKelasResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface KelasService {
    ListKelasResponse createKelas(KelasRequest payload);
    Page<ListKelasResponse> getAllKelas(Pageable page);
    DetailKelasResponse getKelasById(Long id);
    DetailKelasResponse getKelasByNama(String nama);
    DetailKelasResponse updateKelasById(Long id, KelasRequest payload);
    void deleteKelasById(Long id);
}
