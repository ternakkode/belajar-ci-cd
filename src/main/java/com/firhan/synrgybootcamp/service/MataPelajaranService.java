package com.firhan.synrgybootcamp.service;

import com.firhan.synrgybootcamp.presenter.model.request.MataPelajaranRequest;
import com.firhan.synrgybootcamp.presenter.model.response.DetailMataPelajaranResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListMataPelajaranResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MataPelajaranService {
    ListMataPelajaranResponse createMataPelajaran(MataPelajaranRequest payload);
    Page<ListMataPelajaranResponse> getAllMataPelajaran(Pageable page);
    DetailMataPelajaranResponse getMataPelajaranById(Long id);
    DetailMataPelajaranResponse updateMataPelajaranByiD(Long id, MataPelajaranRequest payload);
    void deleteMataPelajaranById(Long id);
}
