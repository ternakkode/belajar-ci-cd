package com.firhan.synrgybootcamp.service;

import com.firhan.synrgybootcamp.presenter.model.response.DetailWaktuResponse;
import com.firhan.synrgybootcamp.presenter.model.request.WaktuRequest;
import com.firhan.synrgybootcamp.presenter.model.response.ListWaktuResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface WaktuService {
    ListWaktuResponse createWaktu(WaktuRequest payload);
    Page<ListWaktuResponse> getAllWaktu(Pageable page);
    DetailWaktuResponse getWaktuById(Long id);
    DetailWaktuResponse updateWaktuById(Long id, WaktuRequest payload);
    void deleteWaktuById(Long id);
}
