package com.firhan.synrgybootcamp.service;

import com.firhan.synrgybootcamp.presenter.model.request.SiswaRequest;
import com.firhan.synrgybootcamp.presenter.model.response.DetailSiswaResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListSiswaResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SiswaService {
    ListSiswaResponse createSiswa(SiswaRequest payload);
    Page<ListSiswaResponse> getAllSiswa(String nama, Pageable page);
    DetailSiswaResponse getSiswaById(Long id);
    DetailSiswaResponse updateSiswaById(Long id, SiswaRequest payload);
    void deleteSiswaById(Long id);
}
