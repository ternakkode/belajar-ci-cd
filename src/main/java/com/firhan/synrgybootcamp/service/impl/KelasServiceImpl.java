package com.firhan.synrgybootcamp.service.impl;

import com.firhan.synrgybootcamp.entity.Kelas;
import com.firhan.synrgybootcamp.exception.ResourceNotFoundException;
import com.firhan.synrgybootcamp.presenter.model.request.KelasRequest;
import com.firhan.synrgybootcamp.presenter.model.response.DetailKelasResponse;
import com.firhan.synrgybootcamp.presenter.model.response.JadwalKelasResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListKelasResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListSiswaResponse;
import com.firhan.synrgybootcamp.repository.KelasRepository;
import com.firhan.synrgybootcamp.service.KelasService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class KelasServiceImpl implements KelasService {

    KelasRepository kelasRepository;

    public KelasServiceImpl(KelasRepository kelasRepository) {
        this.kelasRepository = kelasRepository;
    }

    @Override
    public ListKelasResponse createKelas(KelasRequest payload) {
        Kelas kelasResult = kelasRepository.save(
                Kelas.builder()
                        .nama(payload.getNama())
                        .build()
        );

        return entityToListResponse(kelasResult);
    }

    @Override
    public Page<ListKelasResponse> getAllKelas(Pageable page) {
        Page<Kelas> plainPagedKelas = kelasRepository.findAll(page);

        return new PageImpl<>(
                plainPagedKelas.stream().map(
                        kelas -> entityToListResponse(kelas)
                ).collect(Collectors.toList()), page, plainPagedKelas.getTotalElements()
        );
    }

    @Override
    public DetailKelasResponse getKelasByNama(String nama) {
        Kelas kelas = kelasRepository.findFirstByNama(nama)
                .orElseThrow(() -> new ResourceNotFoundException("kelas data not found"));

        return entityToDetailResponse(kelas);
    }

    @Override
    public DetailKelasResponse getKelasById(Long id) {
        Kelas kelas = kelasRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("kelas data not found"));

        return entityToDetailResponse(kelas);
    }

    @Override
    public DetailKelasResponse updateKelasById(Long id, KelasRequest payload) {
        Kelas kelas = kelasRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("kelas data not found"));

        kelas.setNama(payload.getNama());
        Kelas updatedKelas = kelasRepository.save(kelas);

        return entityToDetailResponse(updatedKelas);
    }

    @Override
    public void deleteKelasById(Long id) {
        Kelas kelas = kelasRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("kelas data not found"));

        kelasRepository.delete(kelas);
    }

    private ListKelasResponse entityToListResponse(Kelas kelas) {
        return ListKelasResponse.builder()
                .id(kelas.getId())
                .nama(kelas.getNama())
                .build();
    }

    private DetailKelasResponse entityToDetailResponse(Kelas kelas) {
        return DetailKelasResponse.builder()
                .id(kelas.getId())
                .nama(kelas.getNama())
                .siswa(kelas.getSiswa().stream().map(
                        oldSiswa -> ListSiswaResponse.
                                builder()
                                .id(oldSiswa.getId())
                                .nama(oldSiswa.getNama())
                                .build()
                        ).collect(Collectors.toSet())
                ).jadwal(kelas.getJadwal().stream().map(
                                oldJadwal -> JadwalKelasResponse.builder()
                                        .hari(oldJadwal.getHari().name())
                                        .waktu(oldJadwal.getWaktu().getDetail())
                                        .mataPelajaran(oldJadwal.getMataPelajaran().getNama())
                                        .pendidik(oldJadwal.getPendidik().getNama())
                                        .build()
                        ).collect(Collectors.toSet())
                ).build();
    }
}
