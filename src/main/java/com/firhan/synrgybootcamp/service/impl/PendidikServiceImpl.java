package com.firhan.synrgybootcamp.service.impl;

import com.firhan.synrgybootcamp.entity.Pendidik;
import com.firhan.synrgybootcamp.entity.Siswa;
import com.firhan.synrgybootcamp.exception.ResourceNotFoundException;
import com.firhan.synrgybootcamp.presenter.model.request.PendidikRequest;
import com.firhan.synrgybootcamp.presenter.model.response.*;
import com.firhan.synrgybootcamp.repository.PendidikRepository;
import com.firhan.synrgybootcamp.service.PendidikService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class PendidikServiceImpl implements PendidikService {

    private PendidikRepository pendidikRepository;

    public PendidikServiceImpl(PendidikRepository pendidikRepository) {
        this.pendidikRepository = pendidikRepository;
    }

    @Override
    public ListPendidikResponse createPendidik(PendidikRequest payload) {
        Pendidik pendidikResult = pendidikRepository.save(
                Pendidik.builder()
                        .nama(payload.getNama())
                        .build()
        );

        return entityToListResponse(pendidikResult);
    }

    @Override
    public Page<ListPendidikResponse> getAllPendidik(String nama, Pageable page) {
        Page<Pendidik> plainPagedPendidik = nama != null
                ? pendidikRepository.findByNama(nama, page)
                : pendidikRepository.findAll(page);

        return new PageImpl<>(
                plainPagedPendidik.stream().map(
                        pendidik -> entityToListResponse(pendidik)
                ).collect(Collectors.toList()), page, plainPagedPendidik.getTotalElements()
        );
    }

    @Override
    public DetailPendidikResponse getPendidikById(Long id) {
        Pendidik pendidik = pendidikRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("pendidik data not found"));

        return entityToDetailResponse(pendidik);
    }

    @Override
    public DetailPendidikResponse updatePendidikById(Long id, PendidikRequest payload) {
        Pendidik pendidik = pendidikRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("pendidik data not found"));

        pendidik.setNama(payload.getNama());
        Pendidik updatedPendidik = pendidikRepository.save(pendidik);

        return entityToDetailResponse(updatedPendidik);
    }

    @Override
    public void deletePendidikByid(Long id) {
        Pendidik pendidik = pendidikRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("pendidik data not found"));

        pendidikRepository.delete(pendidik);
    }

    private ListPendidikResponse entityToListResponse(Pendidik pendidik) {
        return ListPendidikResponse.builder()
                .id(pendidik.getId())
                .nama(pendidik.getNama())
                .build();
    }

    private DetailPendidikResponse entityToDetailResponse(Pendidik pendidik) {
        return DetailPendidikResponse.builder()
                .id(pendidik.getId())
                .nama(pendidik.getNama())
                .jadwal(pendidik.getJadwal().stream().map(
                        oldJadwal -> JadwalPendidikResponse.builder()
                                .kelas(oldJadwal.getKelas().getNama())
                                .mataPelajaran(oldJadwal.getMataPelajaran().getNama())
                                .hari(oldJadwal.getHari().name())
                                .waktu(oldJadwal.getWaktu().getDetail())
                                .build()
                        ).collect(Collectors.toSet())
                ).build();
    }
}
