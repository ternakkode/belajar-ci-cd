package com.firhan.synrgybootcamp.service.impl;

import com.firhan.synrgybootcamp.entity.Waktu;
import com.firhan.synrgybootcamp.exception.ResourceNotFoundException;
import com.firhan.synrgybootcamp.presenter.model.response.DetailWaktuResponse;
import com.firhan.synrgybootcamp.presenter.model.response.JadwalWaktuResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListWaktuResponse;
import com.firhan.synrgybootcamp.presenter.model.request.WaktuRequest;
import com.firhan.synrgybootcamp.repository.WaktuRepository;
import com.firhan.synrgybootcamp.service.WaktuService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class WaktuServiceImpl implements WaktuService {

    private WaktuRepository waktuRepository;

    public WaktuServiceImpl(WaktuRepository waktuRepository) {
        this.waktuRepository = waktuRepository;
    }

    @Override
    public ListWaktuResponse createWaktu(WaktuRequest payload) {
        Waktu waktuResult = waktuRepository.save(
                Waktu.builder()
                        .detail(payload.getDetail())
                        .build()
        );

        return entitiyToListResponse(waktuResult);
    }

    @Override
    public Page<ListWaktuResponse> getAllWaktu(Pageable page) {
        Page<Waktu> plainPagedWaktu = waktuRepository.findAll(page);

        return new PageImpl<>(
                plainPagedWaktu.stream().map(
                        waktu -> entitiyToListResponse(waktu)
                ).collect(Collectors.toList()), page, plainPagedWaktu.getNumberOfElements()
        );
    }

    @Override
    public DetailWaktuResponse getWaktuById(Long id) {
        Waktu waktu = waktuRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("waktu data not found"));

        return entityToDetailResponse(waktu);
    }

    @Override
    public DetailWaktuResponse updateWaktuById(Long id, WaktuRequest payload) {
        Waktu waktu = waktuRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("waktu data not found"));

        waktu.setDetail(payload.getDetail());
        Waktu updatedWaktu = waktuRepository.save(waktu);

        return entityToDetailResponse(updatedWaktu);
    }

    @Override
    public void deleteWaktuById(Long id) {
        Waktu waktu = waktuRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("waktu data not found"));

        waktuRepository.delete(waktu);
    }

    private ListWaktuResponse entitiyToListResponse(Waktu waktu) {
        return ListWaktuResponse.builder()
                .id(waktu.getId())
                .detail(waktu.getDetail())
                .build();
    }

    private DetailWaktuResponse entityToDetailResponse(Waktu waktu) {
        return DetailWaktuResponse.builder()
                .id(waktu.getId())
                .detail(waktu.getDetail())
                .jadwal(
                        waktu.getJadwal().stream().map(
                                oldJadwal -> JadwalWaktuResponse.builder()
                                        .kelas(oldJadwal.getKelas().getNama())
                                        .mataPelajaran(oldJadwal.getMataPelajaran().getNama())
                                        .pendidik(oldJadwal.getPendidik().getNama())
                                        .hari(oldJadwal.getHari().name())
                                        .build()
                        ).collect(Collectors.toSet())
                ).build();
    }
}
