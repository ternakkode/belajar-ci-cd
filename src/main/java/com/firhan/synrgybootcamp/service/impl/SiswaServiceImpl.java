package com.firhan.synrgybootcamp.service.impl;

import com.firhan.synrgybootcamp.entity.Kelas;
import com.firhan.synrgybootcamp.entity.Siswa;
import com.firhan.synrgybootcamp.exception.ResourceNotFoundException;
import com.firhan.synrgybootcamp.presenter.model.request.SiswaRequest;
import com.firhan.synrgybootcamp.presenter.model.response.DetailSiswaResponse;
import com.firhan.synrgybootcamp.presenter.model.response.JadwalSiswaResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListKelasResponse;
import com.firhan.synrgybootcamp.presenter.model.response.ListSiswaResponse;
import com.firhan.synrgybootcamp.repository.KelasRepository;
import com.firhan.synrgybootcamp.repository.SiswaRepository;
import com.firhan.synrgybootcamp.service.SiswaService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class SiswaServiceImpl implements SiswaService {

    private SiswaRepository siswaRepository;
    private KelasRepository kelasRepository;

    public SiswaServiceImpl(SiswaRepository siswaRepository, KelasRepository kelasRepository) {
        this.siswaRepository = siswaRepository;
        this.kelasRepository = kelasRepository;
    }

    @Override
    public ListSiswaResponse createSiswa(SiswaRequest payload) {
        Kelas kelas = kelasRepository.findById(payload.getKelasId())
                .orElseThrow(() -> new ResourceNotFoundException("kelas data not found"));

        Siswa siswaResult = siswaRepository.save(
                Siswa.builder()
                        .nama(payload.getNama())
                        .kelas(kelas)
                        .build()
        );

        return entityToListResponse(siswaResult);
    }

    @Override
    public Page<ListSiswaResponse> getAllSiswa(String nama, Pageable page) {
        Page<Siswa> plainPagedSiswa = nama != null
                ? siswaRepository.findByNama(nama, page)
                : siswaRepository.findAll(page);

        return new PageImpl<>(
                plainPagedSiswa.stream().map(
                        siswa -> entityToListResponse(siswa)
                ).collect(Collectors.toList()), page, plainPagedSiswa.getTotalElements()
        );
    }

    @Override
    public DetailSiswaResponse getSiswaById(Long id) {
        Siswa siswa = siswaRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("siswa data not found"));

        return entityToDetailResponse(siswa);
    }

    @Override
    public DetailSiswaResponse updateSiswaById(Long id, SiswaRequest payload) {
        Siswa siswa = siswaRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("siswa data not found"));

        Kelas kelas = kelasRepository.findById(payload.getKelasId())
                .orElseThrow(() -> new ResourceNotFoundException("kelas data not found"));

        siswa.setNama(payload.getNama());
        siswa.setKelas(kelas);
        Siswa updatedSiswa = siswaRepository.save(siswa);

        return entityToDetailResponse(updatedSiswa);
    }

    @Override
    public void deleteSiswaById(Long id) {
        Siswa siswa = siswaRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("siswa data not found"));

        siswaRepository.delete(siswa);
    }

    private ListSiswaResponse entityToListResponse(Siswa siswa) {
        return ListSiswaResponse.builder()
                .id(siswa.getId())
                .nama(siswa.getNama())
                .kelas(
                        ListKelasResponse.builder()
                                .id(siswa.getKelas().getId())
                                .nama(siswa.getKelas().getNama())
                                .build()
                ).build();
    }

    private DetailSiswaResponse entityToDetailResponse(Siswa siswa) {
        return DetailSiswaResponse.builder()
                .id(siswa.getId())
                .nama(siswa.getNama())
                .kelas(
                        ListKelasResponse.builder()
                                .id(siswa.getKelas().getId())
                                .nama(siswa.getKelas().getNama())
                                .build()
                ).jadwal(
                        siswa.getKelas().getJadwal().stream().map(
                                oldJadwal -> JadwalSiswaResponse.builder()
                                        .hari(oldJadwal.getHari().name())
                                        .waktu(oldJadwal.getWaktu().getDetail())
                                        .kelas(oldJadwal.getKelas().getNama())
                                        .pendidik(oldJadwal.getPendidik().getNama())
                                        .mataPelajaran(oldJadwal.getMataPelajaran().getNama())
                                        .build()
                        ).collect(Collectors.toSet())
                ).build();
    }
}
