package com.firhan.synrgybootcamp.service.impl;

import com.firhan.synrgybootcamp.entity.*;
import com.firhan.synrgybootcamp.exception.ResourceNotFoundException;
import com.firhan.synrgybootcamp.presenter.model.request.JadwalRequest;
import com.firhan.synrgybootcamp.presenter.model.response.*;
import com.firhan.synrgybootcamp.repository.*;
import com.firhan.synrgybootcamp.service.JadwalService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class JadwalServiceIml implements JadwalService {

    private JadwalRepository jadwalRepository;
    private KelasRepository kelasRepository;
    private MataPelajaranRepository mataPelajaranRepository;
    private PendidikRepository pendidikRepository;
    private WaktuRepository waktuRepository;

    public JadwalServiceIml(JadwalRepository jadwalRepository, KelasRepository kelasRepository, MataPelajaranRepository mataPelajaranRepository, PendidikRepository pendidikRepository, WaktuRepository waktuRepository) {
        this.jadwalRepository = jadwalRepository;
        this.kelasRepository = kelasRepository;
        this.mataPelajaranRepository = mataPelajaranRepository;
        this.pendidikRepository = pendidikRepository;
        this.waktuRepository = waktuRepository;
    }

    @Override
    public JadwalResponse createJadwal(JadwalRequest payload) {
        Kelas kelas = kelasRepository.findById(payload.getKelasId())
                .orElseThrow(() -> new ResourceNotFoundException("kelas data not found"));
        MataPelajaran mataPelajaran = mataPelajaranRepository.findById(payload.getMataPelajaranId())
                .orElseThrow(() -> new ResourceNotFoundException("mata pelajaran data not found"));
        Pendidik pendidik = pendidikRepository.findById(payload.getPendidikId())
                .orElseThrow(() -> new ResourceNotFoundException("pendidik data not found"));
        Waktu waktu = waktuRepository.findById(payload.getWaktuId())
                .orElseThrow(() -> new ResourceNotFoundException("waktu data not found"));

        Jadwal jadwalResult = Jadwal.builder()
                .kelas(kelas)
                .mataPelajaran(mataPelajaran)
                .pendidik(pendidik)
                .hari(Hari.valueOf(payload.getHari()))
                .waktu(waktu)
                .build();

        jadwalRepository.save(jadwalResult);

        return entityToResponse(jadwalResult);
    }

    @Override
    public Page<JadwalResponse> getAllJadwal(Pageable page) {
        Page<Jadwal> plainPagedJadwal = jadwalRepository.findAll(page);

        return new PageImpl<>(
                plainPagedJadwal.stream().map(
                        jadwal -> entityToResponse(jadwal)
                ).collect(Collectors.toList()), page, plainPagedJadwal.getTotalElements()
        );
    }

    @Override
    public JadwalResponse getJadwalById(Long id) {
        Jadwal jadwal = jadwalRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("jadwal data not found"));

        return entityToResponse(jadwal);
    }

    @Override
    public JadwalResponse updateJadwalById(Long id, JadwalRequest payload) {
        Jadwal jadwal = jadwalRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("jadwal data not found"));
        Kelas kelas = kelasRepository.findById(payload.getKelasId())
                .orElseThrow(() -> new ResourceNotFoundException("kelas data not found"));
        MataPelajaran mataPelajaran = mataPelajaranRepository.findById(payload.getMataPelajaranId())
                .orElseThrow(() -> new ResourceNotFoundException("mata pelajaran data not found"));
        Pendidik pendidik = pendidikRepository.findById(payload.getPendidikId())
                .orElseThrow(() -> new ResourceNotFoundException("pendidik data not found"));
        Waktu waktu = waktuRepository.findById(payload.getWaktuId())
                .orElseThrow(() -> new ResourceNotFoundException("waktu data not found"));

        jadwal.setKelas(kelas);
        jadwal.setMataPelajaran(mataPelajaran);
        jadwal.setPendidik(pendidik);
        jadwal.setWaktu(waktu);
        jadwal.setHari(Hari.valueOf(payload.getHari()));
        Jadwal updatedJadwal = jadwalRepository.save(jadwal);

        return entityToResponse(updatedJadwal);
    }

    @Override
    public void deleteJadwalByid(Long id) {
        Jadwal jadwal = jadwalRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("jadwal data not found"));

        jadwalRepository.delete(jadwal);
    }

    private JadwalResponse entityToResponse(Jadwal jadwal) {
        return JadwalResponse.builder()
                .id(jadwal.getId())
                .hari(jadwal.getHari().name())
                .waktu(ListWaktuResponse.builder()
                        .id(jadwal.getWaktu().getId())
                        .detail(jadwal.getWaktu().getDetail())
                        .build()
                ).kelas(ListKelasResponse.builder()
                        .id(jadwal.getKelas().getId())
                        .nama(jadwal.getKelas().getNama())
                        .build()
                ).mataPelajaran(ListMataPelajaranResponse.builder()
                        .id(jadwal.getMataPelajaran().getId())
                        .nama(jadwal.getMataPelajaran().getNama())
                        .build()
                ).mataPelajaran(ListMataPelajaranResponse.builder()
                        .id(jadwal.getMataPelajaran().getId())
                        .nama(jadwal.getMataPelajaran().getNama())
                        .build()
                ).pendidik(ListPendidikResponse.builder()
                        .id(jadwal.getPendidik().getId())
                        .nama(jadwal.getPendidik().getNama())
                        .build()
                ).build();
    }
}
