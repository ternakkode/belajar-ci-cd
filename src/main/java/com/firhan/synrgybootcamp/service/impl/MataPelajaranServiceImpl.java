package com.firhan.synrgybootcamp.service.impl;

import com.firhan.synrgybootcamp.entity.MataPelajaran;
import com.firhan.synrgybootcamp.exception.ResourceNotFoundException;
import com.firhan.synrgybootcamp.presenter.model.request.MataPelajaranRequest;
import com.firhan.synrgybootcamp.presenter.model.response.*;
import com.firhan.synrgybootcamp.repository.MataPelajaranRepository;
import com.firhan.synrgybootcamp.service.MataPelajaranService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class MataPelajaranServiceImpl implements MataPelajaranService {

    private MataPelajaranRepository mataPelajaranRepository;

    public MataPelajaranServiceImpl(MataPelajaranRepository mataPelajaranRepository) {
        this.mataPelajaranRepository = mataPelajaranRepository;
    }

    @Override
    public ListMataPelajaranResponse createMataPelajaran(MataPelajaranRequest payload) {
        MataPelajaran mataPelajaranResult = mataPelajaranRepository.save(
                MataPelajaran.builder()
                        .nama(payload.getNama())
                        .build()
        );

        return entitiyToListResponse(mataPelajaranResult);
    }

    @Override
    public Page<ListMataPelajaranResponse> getAllMataPelajaran(Pageable page) {
        Page<MataPelajaran> plainPagedMataPelajaran = mataPelajaranRepository.findAll(page);

        return new PageImpl<>(
                plainPagedMataPelajaran.stream().map(
                        mataPelajaran -> entitiyToListResponse(mataPelajaran)
                ).collect(Collectors.toList()), page, plainPagedMataPelajaran.getTotalElements()
        );
    }

    @Override
    public DetailMataPelajaranResponse getMataPelajaranById(Long id) {
        MataPelajaran mataPelajaran = mataPelajaranRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("mata pelajaran data not found"));

        return entityToDetailResponse(mataPelajaran);
    }

    @Override
    public DetailMataPelajaranResponse updateMataPelajaranByiD(Long id, MataPelajaranRequest payload) {
       MataPelajaran mataPelajaran = mataPelajaranRepository.findById(id)
               .orElseThrow(() -> new ResourceNotFoundException("mata pelajaran data not found"));

       mataPelajaran.setNama(payload.getNama());
       MataPelajaran updatedMataPelajaran = mataPelajaranRepository.save(mataPelajaran);

       return entityToDetailResponse(updatedMataPelajaran);
    }

    @Override
    public void deleteMataPelajaranById(Long id) {
        MataPelajaran mataPelajaran = mataPelajaranRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("mata pelajaran data not found"));

        mataPelajaranRepository.delete(mataPelajaran);
    }

    private ListMataPelajaranResponse entitiyToListResponse(MataPelajaran mataPelajaran) {
        return ListMataPelajaranResponse.builder()
                .id(mataPelajaran.getId())
                .nama(mataPelajaran.getNama())
                .build();
    }

    private DetailMataPelajaranResponse entityToDetailResponse(MataPelajaran mataPelajaran) {
        return DetailMataPelajaranResponse.builder()
                .id(mataPelajaran.getId())
                .nama(mataPelajaran.getNama())
                .jadwal(
                        mataPelajaran.getJadwal().stream().map(
                                oldJadwal -> JadwalMataPelajaranResponse.builder()
                                        .kelas(oldJadwal.getKelas().getNama())
                                        .pendidik(oldJadwal.getPendidik().getNama())
                                        .hari(oldJadwal.getHari().name())
                                        .waktu(oldJadwal.getWaktu().getDetail())
                                        .build()
                        ).collect(Collectors.toSet())
                ).build();
    }
}
