package com.firhan.synrgybootcamp.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

@AllArgsConstructor
@Getter
@Setter
public class Pagination {
    long total;
    int perPage;
    int currentPage;
    int totalPage;

    public Pagination(Page pageable) {
        this.total = pageable.getTotalElements();
        this.perPage = pageable.getPageable().getPageSize();
        this.currentPage = pageable.getPageable().getPageNumber();
        this.totalPage = pageable.getTotalPages();
    }
}
