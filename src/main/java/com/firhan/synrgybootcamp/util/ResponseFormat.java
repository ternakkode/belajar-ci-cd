package com.firhan.synrgybootcamp.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseFormat {
    private String message;
    private Object data;
    private Pagination pagination;
}
